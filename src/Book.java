import java.text.NumberFormat;

public class Book implements Printable{

    private String title;
    private double price;
    

    public Book() {
        this.title = "";
        this.price = 0;
    }

    public Book(String description, double price) {
        this.title = description;
        this.price = price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormatted() {
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        return currency.format(price);
    }
    
    @Override
    public String getText()
    {
    	return( String.format("Title: %s, Price: $%s", getTitle(), getPrice()) ); 
    } //end method getText
    
    // TODO: As part of implementing the Printable interface,
    //       you must write a concrete implementation of the
    //       Printable interface's abstract getText method

} // end class Book