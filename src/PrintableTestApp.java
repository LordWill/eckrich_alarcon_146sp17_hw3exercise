import java.util.*;

/**
 * Application to test polymorphic behavior of Book and Student classes
 * that have correctly implemented the Printable interface
 * 
 * @author Lastname1 Lastname2
 * @version CSCI 146 Spring 2017 HW3 In-class Exercise
 */
public class PrintableTestApp {

	/**
	 * Main method for the application
	 * @param args
	 */
    public static void main(String args[]) {
    	
    	Scanner input = new Scanner( System.in );
    	
    	System.out.println( "Enter the student's first name:" );
    	String firstName = input.nextLine();
    	
    	System.out.println( "Enter the student's last name:" );
    	String lastName = input.nextLine();
    	
    	System.out.println( "Enter the title of the book:" );
    	String bookTitle = input.nextLine();
    	
		System.out.println( 
			"Enter the book price as a decimal value (do not use the $ sign):" );
		
		// TODO: declare a double, bookPrice, with default value of 9.99
		double bookPrice = 9.99;
    	
    	// TODO: Write a try statement that attempts to read in a double
    	//       value input by the user. Write catch blocks that handle
    	//       two possible exceptions (a RuntimeException and 
    	//       an InputMismatchException), but remember that the catch
    	//       order does matter (more specific exception types must be 
    	//       caught before less specific exceptions)! In addition, write
    	//       a finally block that closes the Scanner object.
		try
		{
			bookPrice = input.nextDouble();
		} // end try block
		catch (InputMismatchException e)
		{
			System.err.println("InputMismatchException caught - Invalid data entered!");
			System.err.println("Using default price of 9.99 instead.");
		} // end InputMismatchException block
		catch (RuntimeException e)
		{
			System.err.print(e.getStackTrace());
		} // end RuntimeException block
		finally
		{
			input.close();
		} // end finally block

		
		// TODO: Declare and create a 2-element array of Printable objects
		Printable[] printables = new Printable[2];
		
		// TODO: Assign a new Student object (initialized using last & first  
		// name data from above) to one of the elements of the Printable array
		printables[0] = new Student( lastName, firstName );
		
		// TODO: Assign a new Book object (initialized using title & price
		// data entered above) to the other element of the Printable array       
		printables[1] = new Book( bookTitle, bookPrice );

		// print a blank line (for formatting purposes)
        System.out.println(); /* Don't change or delete this line */
        
        
        // TODO: Write an enhanced for loop that iterates through each element
        // of your array of Printable objects, as described in the instructions
        for( Printable printableObj:printables )
        {
        	if ( printableObj instanceof Book )
        	{
        		System.out.print("Book - ");
        	} // end if block
        	else
        	{
        		System.out.print("Student - ");
        	} // end else block
        	printToConsole(printableObj);
        } // end enhanced for loop
         
    } // end method main

    /**
     * Prints the String output of the passed Printable object's getText method
     * @param obj
     */
    private static void printToConsole( Printable obj ) 
    {
        System.out.println( obj.getText() );
    } // end static method printToConsole
    
} // end class PrintableTestApp
