public class Student implements Printable {
	
    private String firstName;
    private String lastName;

    public Student(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    @Override
    public String getText()
    {
    	return( String.format("Name: %s, %s", lastName, firstName) ); 
    } //end method getText
    
} // end class Student